#!/usr/bin/env bash

module load opencv
if [ ! -d .venv ]; then
    echo "Creating virtual environment..."
    virtualenv .venv
    source .venv/bin/activate
    pip install -U mosaic-library oculus-python
else
    source .venv/bin/activate
fi
echo "Environment setup"
