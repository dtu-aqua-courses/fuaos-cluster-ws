import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Designate the path to the data file
filename = "data/ping_recording_20240305T230023.csv"
# Read in the data
df = pd.read_csv(filename)

# Unpack the echogram
echogram = df.iloc[:,-200:].to_numpy()

# Get the start range and the scan length
range_start = df.loc[0, 'Scan Start']
range_length = df.loc[0, 'Scan Length']

# Compute the ranging for each sample
ranges = np.linspace(range_start, range_start + range_length, 200)

# Get some label information for the echogram
sample_ticks = np.linspace(0, 199, 10)
sample_tick_labels = np.linspace(range_start, range_length + range_start, 10)
sample_tick_labels = [f"{sample:.0f}" for sample in sample_tick_labels]

# Display the echogram
fig, ax = plt.subplots()
ax.imshow(echogram.T)
ax.set_xlabel("Ping Number")
ax.set_ylabel("Ranging (mm)")
ax.set_yticks(sample_ticks)
ax.set_yticklabels(sample_tick_labels)
plt.show()
