import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from typing import Tuple


def detect_nearest(echogram: np.ndarray, ranges: np.ndarray, threshold: int, min_range: float) -> Tuple[np.ndarray, np.ndarray]:
    """
    Detect single targets in acoustic imagery.
    :param echogram: The echogram with dimensions [range, ping].
    :param ranges: The vector of range coordinates for the range dimension of the echogram.
    :param threshold: The minimum threshold required to detect a target
    :param min_range: The minimum range required to detect a target
    :return: Tuple containing an array of the ranges for detected targets and an array for the respective sample index.
    """
    # Find the sample index for the closest range sample to the minimum range
    min_range_idx = np.argmin(np.abs(ranges - min_range))

    # Find samples in echogram where the return echo is greater than the provided threshold
    targets = echogram >= threshold
    targets[:min_range_idx] = False  # set the targets closer than the min range to be False

    # This method finds the first valid detection after the min range for each ping
    idx = np.argmax(targets, axis=0)

    return ranges[idx], idx



# Designate the path to the data file
filename = "data/ping_recording_20240305T230023.csv"
# Read in the data
df = pd.read_csv(filename)

# Unpack the echogram
echogram = df.iloc[:,-200:].to_numpy()

# Get the start range and the scan length
range_start = df.loc[0, 'Scan Start']
range_length = df.loc[0, 'Scan Length']

# Compute the ranging for each sample
ranges = np.linspace(range_start, range_start + range_length, 200)
# Detect the ranges
target_range, target_idx = detect_nearest(echogram.T, ranges, 150, 500)
# Compare my calculated range to the range estimated by the transducer
distances = df.loc[:,"Distance"].to_numpy()
distances_idx = np.argmin(np.abs(ranges[:, None] - distances[None, :]), axis=0)
error = np.sqrt(np.mean((distances - target_range)**2))
print(f"RMS error: {error:.2f} mm")

# Get some label information for the echogram
sample_ticks = np.linspace(0, 199, 10)
sample_tick_labels = np.linspace(range_start, range_length + range_start, 10)
sample_tick_labels = [f"{sample:.0f}" for sample in sample_tick_labels]

# Display the echogram
fig, ax = plt.subplots(figsize=(8, 10))
ax.imshow(echogram.T)
ax.set_xlabel("Ping Number")
ax.set_ylabel("Ranging (mm)")
ax.set_yticks(sample_ticks)
ax.set_yticklabels(sample_tick_labels)
ax.plot(target_idx - 0.5, 'r', label="My distance estimate.", linewidth=4)
ax.plot(distances_idx - 0.5, 'm', label="Transducer estimate.", linewidth=4)
ax.legend(loc='upper left', bbox_to_anchor=(1, 1))
plt.show()
