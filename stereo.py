import cv2
import yaml
import numpy as np
from pathlib import Path


def load_calibration(file_name: str) -> tuple:
    """
    Loads calibration from file.
    """
    # Load the YAML file
    with open(file_name, "r") as f:
        data = yaml.safe_load(f)

    size = data["Size"]
    # Convert matrices into NumPy arrays
    K_LEFT = np.array(data["K_LEFT"]["data"])
    D_LEFT = np.array(data["D_LEFT"]["data"])
    K_RIGHT = np.array(data["K_RIGHT"]["data"])
    D_RIGHT = np.array(data["D_RIGHT"]["data"])
    R = np.array(data["R"]["data"])
    T = np.array(data["T"]["data"])

    # Reshape them to match their intended structure
    K_LEFT = K_LEFT.reshape((data["K_LEFT"]["rows"], data["K_LEFT"]["cols"]))
    D_LEFT = D_LEFT.reshape((data["D_LEFT"]["rows"], data["D_LEFT"]["cols"]))
    K_RIGHT = K_RIGHT.reshape((data["K_RIGHT"]["rows"], data["K_RIGHT"]["cols"]))
    D_RIGHT = D_RIGHT.reshape((data["D_RIGHT"]["rows"], data["D_RIGHT"]["cols"]))
    R = R.reshape((data["R"]["rows"], data["R"]["cols"]))
    T = T.reshape((data["T"]["rows"], data["T"]["cols"]))

    # Return the arrays as a tuple
    return size, K_LEFT, D_LEFT, K_RIGHT, D_RIGHT, R, T

def create_output(vertices, colors, filename):
    """
    Packs points and colors into a .ply point cloud file.
    """
    colors = colors.reshape(-1, 3)
    vertices = np.hstack([vertices.reshape(-1,3), colors])

    ply_header = '''ply
        format ascii 1.0
        element vertex %(vert_num)d
        property float x
        property float y
        property float z
        property uchar red
        property uchar green
        property uchar blue
        end_header
    '''

    with open(filename, 'w') as f:
        f.write(ply_header % dict(vert_num=len(vertices)))
        np.savetxt(f, vertices, '%f %f %f %d %d %d')

def main():
    # Specify the image path as a Path variable.
    image_path = Path("data/stereo/sbs_20210929T080424_751_12256.png")

    # Read the image using OpenCV (takes string as input)
    img_sbs = cv2.imread(str(image_path))
    # The image pair are concatenated side by side, so get the width and divide by 2
    width_sbs = img_sbs.shape[1]
    width_single = int(width_sbs / 2)
    img_color_left = img_sbs[:, :width_single, :]
    img_color_right = img_sbs[:, width_single:, :]

    # Convert the image pairs to grayscale for block feature matching purposes.
    img_left = cv2.cvtColor(img_color_left, cv2.COLOR_BGR2GRAY)
    img_right = cv2.cvtColor(img_color_right, cv2.COLOR_BGR2GRAY)

    # Specify the calibration file
    calibration_file = "data/stereo/zed_calibration.yaml"
    # Load it in
    img_size, K_left, D_left, K_right, D_right, R, T = load_calibration(calibration_file)
    # Scale the translation vector to be in m instead of mm.
    T = T / 1000.0

    # Obtain the rectification and projection matrices and calculate the disparity-to-depth mapping matrix
    R_left, R_right, P_left, P_right, Q, valid_pixels_ROI_left, valid_pixels_ROI_right = cv2.stereoRectify(K_left,
                                                                                                           D_left,
                                                                                                           K_right,
                                                                                                           D_right,
                                                                                                           img_size,
                                                                                                           R, T,
                                                                                                           alpha=0.0)

    # Calculate the remapping of image coordinates given the camera intrinsic, distortion coefficients,
    # rectification matrices, projection matrices
    map_x_left, map_y_left = cv2.initUndistortRectifyMap(K_left, D_left, R_left, P_left, img_size, cv2.CV_32FC1)
    map_x_right, map_y_right = cv2.initUndistortRectifyMap(K_right, D_right, R_right, P_right, img_size, cv2.CV_32FC1)

    # Undistort and rectify the images by remapping
    rectified_left = cv2.remap(img_left, map_x_left, map_y_left, cv2.INTER_LINEAR)
    rectified_right = cv2.remap(img_right, map_x_right, map_y_right, cv2.INTER_LINEAR)

    # let's downsize to speed things up, cv2.pyrDown is a fast way to do this
    rectified_left = cv2.pyrDown(rectified_left)
    rectified_right = cv2.pyrDown(rectified_right)
    img_color_left = cv2.pyrDown(img_color_left)

    # Semi-Global Block Matching: an algorithm with a lot of parameters
    win_size = 1  # the Sum of Absolute Differences (SAD) window, it's usually good to be a small window.
    min_disp = 16  # minimum disparity to search for
    max_disp = min_disp * 9  # set the maximum disparity to be 9 x the min (set as you like)
    num_disp = max_disp - min_disp  # Needs to be divisible by 16 for numerical reasons
    sgbm = cv2.StereoSGBM.create(minDisparity=min_disp,
                            numDisparities=num_disp,
                            blockSize=win_size,
                            uniquenessRatio=10, # uniqueness of the matched features (SAD must be greater than this)
                            speckleWindowSize=100,  # Filter out small matches
                            speckleRange=32,
                            disp12MaxDiff=1,  # max allowable difference in disparity between left to right and right to left
                            P1=8 * 3 * win_size ** 2,  # smoothing parameter 1
                            P2=32 * 3 * win_size ** 2,  # smoothing parameter 2
                            mode=cv2.STEREO_SGBM_MODE_HH  # use the full hessian version of the algorithm (best)
                            )

    disparity_map = sgbm.compute(rectified_left, rectified_right).astype(np.float32) / 16.0 # disparity is scaled by 16
    points_3D = cv2.reprojectImageTo3D(disparity_map, Q)  # use the Q disparity-to-depth matrix to compute the 3D points

    colors = cv2.cvtColor(img_color_left, cv2.COLOR_BGR2RGB)  # extract the colors for the left image, points are
    # referenced to the left image by convention.
    mask_map = disparity_map > 0.0  # filter for invalid disparities (<=0)
    output_points = points_3D[mask_map]
    output_colors = colors[mask_map]
    create_output(output_points, output_colors, f"{str(image_path.with_suffix('.ply'))}")

    # Show the image and disparity
    cv2.imshow(f"{image_path.name}", img_sbs)
    cv2.imshow('Disparity Map', (disparity_map - min_disp) / num_disp)

    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()