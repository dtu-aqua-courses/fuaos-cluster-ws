import xarray as xr
import matplotlib.pyplot as plt


def mackenzie_sound(T, S, z):
    """
    From lecture notes (acoustic introduction lecture)
    """
    return 1448.96 + 4.591*T - 0.05304*T**2 + 2.374e-4 * T**3 + 1.34*(S - 35) + 0.0163*z + 1.675e-7*z**2 - 0.01025*T*(S - 35) - 7.139e-13*T*z**3

ds = xr.load_dataset("data/GL_PR_PF_3902564.nc")

# Example: Select data for the first time point and all depths
# This is assuming 'TIME' can represent your notion of position or you have specific criteria to select it
selected_time = ds.TIME.isel(TIME=0)

# Now, extract temperature and salinity for this specific time across all depths
T = ds.TEMP.sel(TIME=selected_time).isel(TIME=0)
S = ds.PSAL.sel(TIME=selected_time).isel(TIME=0)
z = ds.DEPTH
c = mackenzie_sound(T, S, z)

fig, ax = plt.subplots(1, 3, sharey=True, figsize=(8,15))
ax[0].plot(T, z)
ax[0].set_ylabel("Depth (m)", fontsize=14)
ax[0].set_xlabel("T (°C)", fontsize=14)
ax[1].plot(S, z)
ax[1].set_xlabel("S (PSU)", fontsize=14)
ax[2].plot(c, z)
ax[2].invert_yaxis()
ax[2].set_xlabel("c (m/s)", fontsize=14)
plt.show()

# # If you want to select data for a specific depth or range of depths, you can do it like this:
# # For a specific depth, e.g., 100 meters (assuming depth values are in meters and directly indexable)
# depth_value = 100
# temp_at_depth = temp_at_time.sel(DEPTH=depth_value, method='nearest')
# salinity_at_depth = salinity_at_time.sel(DEPTH=depth_value, method='nearest')
#
# # For a range of depths, e.g., from 100 to 200 meters
# depth_start = 100
# depth_end = 200
# temp_in_depth_range = temp_at_time.sel(DEPTH=slice(depth_start, depth_end))
# salinity_in_depth_range = salinity_at_time.sel(DEPTH=slice(depth_start, depth_end))
