from scipy.io import wavfile
from matplotlib import pyplot as plt
import numpy as np

sampling_rate, data = wavfile.read("data/echo_mountain.wav")
dt = 1 / sampling_rate
t = np.arange(0.0, len(data)*dt, dt)
fig, ax = plt.subplots()
ax.plot(t, data)
ax.set_xlabel("Time (s)")
ax.set_ylabel("Amplitude (normalized)")
plt.show()
