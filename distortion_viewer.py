import cv2
import numpy as np
import argparse
from pathlib import Path


class DistortionViewer:
    def __init__(self, img: np.ndarray):
        self._img = img.copy()
        self._h, self._w = self._img.shape[:2]
        cv2.namedWindow("display", cv2.WINDOW_GUI_NORMAL)
        cv2.setWindowTitle("display", "Distortion Viewer")
        self._k1 = 0
        self._k2 = 0
        self._p1 = 0
        self._p2 = 0
        self._k3 = 0
        self._fx = max(self._img.shape)
        self._fy = max(self._img.shape)
        self._w = self._img.shape[1]
        self._h = self._img.shape[0]
        # Create trackbars for distortion coefficients
        cv2.createTrackbar("k1", "display", 50, 100, self._k1_callback)
        cv2.createTrackbar("k2", "display", 50, 100, self._k2_callback)
        cv2.createTrackbar("p1", "display", 50, 100, self._p1_callback)
        cv2.createTrackbar("p2", "display", 50, 100, self._p2_callback)
        cv2.waitKey(1)
        # Add more trackbars as needed for p1, p2, k3, etc.

    def _k1_callback(self, val):
        self._k1 = val / 100 - 0.5  # Example scaling to allow negative values

    def _k2_callback(self, val):
        self._k2 = val / 100 - 0.5

    def _p1_callback(self, val):
        self._p1 = val / 100 - 0.5  # Example scaling to allow negative values

    def _p2_callback(self, val):
        self._p2 = val / 100 - 0.5

    # Implement callbacks for other coefficients

    def undistort(self):
        # Camera matrix
        K = np.array([[self._fx, 0, self._w / 2],
                      [0, self._fy, self._h / 2],
                      [0, 0, 1]], dtype=np.float32)
        # Distortion coefficients
        distCoeffs = np.array([self._k1, self._k2, self._p1, self._p2], dtype=np.float32)
      	# Calculate an optimized K for an undistorted version of the image.
        new_K, _ = cv2.getOptimalNewCameraMatrix(K, distCoeffs, (self._w,self._h), 1.0)
        undistorted_img = cv2.undistort(self._img, K, distCoeffs, None, new_K)
        return undistorted_img

    def play(self):
        k = -1
        cv2.setWindowTitle("display", "Distortion Viewer: Press s / S to save the undistorted image, press ESC to exit.")
        while k != 27:
            undistorted_img = self.undistort()
            # Combine original and undistorted images
            combined_img = np.concatenate((self._img, undistorted_img), axis=1)
            cv2.imshow("display", combined_img)
            k = cv2.waitKey(1)
            if k == ord("s") or k == ord("S"):
                cv2.imwrite(str(args.image.with_name(f"undistorted_k1{self._k1:.2f}_k2{self._k2:.2f}_p1{self._p1:.2f}_p2{self._p1:.2f}_" + args.image.name)), undistorted_img)
        cv2.destroyWindow("display")

# Usage example
if __name__ == '__main__':
    # parse commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("image", type=Path, help="Path to image to be undistorted.")
    args = parser.parse_args()
    args.image.resolve(True)
    src = cv2.imread(str(args.image))  # Replace with your image path
    viewer = DistortionViewer(src)
    viewer.play()
