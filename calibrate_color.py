import cv2
import numpy as np
from cv2 import mcc, ccm
import argparse
from pathlib import Path


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("image", type=Path, help="Path to image.")
    args = parser.parse_args()

    args.image.resolve(True)

    detector = mcc.CCheckerDetector_create()
    img = cv2.imread(str(args.image), cv2.IMREAD_COLOR)
    og = img.copy()
    ret = detector.process(img, ccm.COLORCHECKER_MACBETH, nc=1)

    for checker in detector.getListColorChecker():
        cdraw = mcc.CCheckerDraw_create(checker)
        chartRGB = checker.getChartsRGB()
        out = cdraw.draw(img.copy())
        src = chartRGB[:, 1].copy().reshape(24, 1, 3) / 255.0
        model = cv2.ccm_ColorCorrectionModel(src=src, constcolor=ccm.COLORCHECKER_MACBETH)
        model.setColorSpace(ccm.COLOR_SPACE_sRGB)
        model.setCCM_TYPE(ccm.CCM_3x3)
        model.setDistance(ccm.DISTANCE_CIE2000)
        model.setLinear(ccm.LINEARIZATION_COLORPOLYFIT)
        model.setLinearDegree(3)
        model.setSaturatedThreshold(0, 0.98)
        model.run()
        print(f'Complete fitting of color calibration model, loss({model.getLoss()})')
        print(f'CCM: \n {model.getCCM()})')

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img_f = img.astype(np.float64) / 255.0
    img_f = model.infer(img_f) * 255.0
    img_f[img_f < 0] = 0
    img_f[img_f > 255] = 255
    img = img_f.astype(np.uint8)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.namedWindow("color corrected", cv2.WINDOW_NORMAL)
    cv2.imshow("color corrected", np.concatenate([og, img], axis=1))
    cv2.waitKey()
    cv2.destroyAllWindows()
