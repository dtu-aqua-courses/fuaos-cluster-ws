from mosaicking import mosaic
import numpy as np
import logging
from pathlib import Path
import yaml
import time

# Set a timer for performance timing
tstart = time.perf_counter()
logging.basicConfig(level=logging.DEBUG)

# Define a path to the video data
data_path = Path(f"data/archaeo_6/archaeo_sequence_6_cinfo_camera_image_raw.mp4").resolve()
# Define a path to the output results folder
output_path = Path(f"data/archaeo_6/{data_path.stem}_output").resolve()
# Define the path to a file containing the orientation recorded by the camera's auxiliary orientation sensor.
orientation_path = next(data_path.parent.glob("*.csv")).resolve()
# Define the path to a file containing the time offset between the system clock and the video start time
orientation_time_offset_path = next(data_path.parent.glob("time_offset*.yml")).resolve()
# Define the path to a file defining the calibration parameters of the camera
calibration_path = next(data_path.parent.glob("calibration*.yaml")).resolve()
# Load the time offset
with open(orientation_time_offset_path, "r") as f:
    orientation_time_offset = yaml.safe_load(f)[data_path.name]
# Load the calibration data
with open(calibration_path, "r") as f:
    calibration_data = yaml.safe_load(f)
K = np.array(calibration_data["camera_matrix"]["data"]).reshape((3,3))
D = np.array(calibration_data["distortion_coefficients"]["data"])
# Define some preprocessors for undistorting the image, cropping it a little to avoid dark regions,
#  applying CLAHE equalization, and scaling the image to half it's original size to speed up processing.
preprocessors = (("undistort", {"K": K, "D": D}, None),
                 ("crop", {"roi": (30, 30, 908, 548)}, None),
                  ("clahe", {}, None),
                  ("scaling", {"scaling": 0.5}, None))
# Initialize the mosaic object to detect ORB and SIFT features, perform preprocessing, starting at 00:02:20 in the video
#  and finishing at frame 3383 (00:02:49).
mosaic = mosaic.SequentialMosaic(   data_path=data_path,
                                    project_path=output_path,
                                    feature_types=("ORB",),
                                    intrinsics={"K": K, "D": D},
                                    orientation_path=orientation_path,
                                    orientation_time_offset=orientation_time_offset,
                                    preprocessing_params=preprocessors,
                                    extractor_kwargs={"nFeatures": 4000},
                                    keypoint_roi=True,
                                    epsilon=1e-3,
                                    reader_params={"start": "00:02:20",
                                                   "finish": 3383})
# Perform feature extraction
mosaic.extract_features()
# Perform feature matching between frames
mosaic.match_features()
# Perform registration on matches
mosaic.registration()
# Link the local registrations to the original image
mosaic.global_registration()
# Generate the mosaic in 1024x1024 tiles
mosaic.generate((1024, 1024))
# The mosaic is finished, stop the timer.
tend = time.perf_counter()
print(f"Time took {tend - tstart} seconds")