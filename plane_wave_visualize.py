import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


def data_gen(frame_number, p, plot):
    wxt = w*t[frame_number]
    p = np.real(np.exp(1j * (kx*X + ky*Y - wxt)))
    ax.clear()
    plot = ax.plot_wireframe(X, Y, p, color='b')
    plt.xlabel(r'X', fontsize=18)
    plt.ylabel(r'Y', fontsize=18)
    ax.set_zlabel(r'p_h(x,y,t)', fontsize=18)
    return plot

frames = 36  # number of animation frames
n_periods = 6  # number of periods
nx = 101  # resolution of wave
freq = 50.0  # wave frequency (Hz)
T = 1.0 / freq  # wave period (s)
w = 2.0 * np.pi * freq  # circular wave frequency (rad/s)
c = 1500.0  # wave speed m/s

k = w / c  # wave number
phi = 0.0  # phase
kx = k * np.cos(phi * np.pi / 180.0)  # x component of wave number
ky = k * np.sin(phi * np.pi / 180.0)  # y component of wave number

L = 2.0 * np.pi / k  # Wave length (m)
t_min = 0  # time start
t_max = T * n_periods  # time end
x_max = 3.0 * L  # spatial coverage (number of wavelengths)
x = np.linspace(-x_max, x_max, nx)  # x component of space
y = np.linspace(-x_max, x_max, nx)  # y component of space

X, Y = np.meshgrid(x, y)  # spatial grid for 3D
t = np.linspace(t_min, t_max, frames)  # time vector

p = np.real( np.exp( 1j*( kx*X + ky*Y - w*t[0])))  # waveform = e^(kx*X + ky*Y - wt)i

# plot the wave
fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')
ax.set_position([0, 0, 0.95, 1])
plot = ax.plot_wireframe(X, Y, p)

# animate the wave for varying time
animation = FuncAnimation(fig, data_gen, fargs = (p, plot),
                          frames = frames, interval = 1, blit=False)

# display the animation
plt.show()
