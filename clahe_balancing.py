import numpy as np
from mosaicking.preprocessing import equalize_luminance
import argparse
from pathlib import Path
import cv2


if __name__ == "__main__":
    # parse commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("image", type=Path, help="Path to image to be balanced.")
    args = parser.parse_args()
    args.image.resolve(True)
    # load the color image
    img = cv2.imread(str(args.image), cv2.IMREAD_COLOR)
    # balance each channel independently
    corrected = equalize_luminance(img, clip_limit=3.0, tile_grid_size=(7, 7))
    # display the output window
    cv2.namedWindow("CLAHE comparison", cv2.WINDOW_NORMAL)
    # combine the original and balanced images with a black strip in the middle
    cv2.imshow("CLAHE comparison", np.concatenate((img, np.zeros((img.shape[0], 10, 3), np.uint8), corrected), axis=1))
    # wait for a keypress in the display window
    key = cv2.waitKey()
    # if the key is an "s" for save, then save the output image.
    if key in [ord("s"), ord("S")]:
        cv2.imwrite(args.image.with_name("clahe_" + args.image.name), corrected)
    cv2.destroyAllWindows()
