import cv2
import numpy as np
import json
import argparse
from pathlib import Path

def draw_axes(img, corners, imgpts):
    corner = tuple(corners[0].ravel())
    img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (0,0,255), 3)
    img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0,255,0), 3)
    img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (255,0,0), 3)

    for pt1, pt2 in zip(corners[0:-1], corners[1:]):
        #img = cv2.line(img, tuple(pt1.ravel()), tuple(pt2.ravel()), (0, 255, 0), 1)
        img = cv2.drawMarker(img, tuple(pt1.ravel()), (255, 120, 255), cv2.MARKER_SQUARE, 4, 3)
    img = cv2.drawMarker(img, tuple(pt2.ravel()), (255, 120, 255), cv2.MARKER_SQUARE, 4, 3)

    return img


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("calibration_file", type=Path, help="Path to calibration json file.")
    parser.add_argument("checkerboard_image", type=Path, help="Path to image containing checkerboard.")
    args = parser.parse_args()

    args.calibration_file.resolve(True)
    args.checkerboard_image.resolve(True)

    # Open a calibration file generatied by mosaicking.calibration and load the data
    with open(args.calibration_file, "r") as f:
        calibration = json.load(f)

    # Intrinsic Matrix
    K = np.array(calibration["K"]).reshape((3,3))
    # Principal Point
    pp = K[:2, -1]
    # Distortion Matrix
    D = np.array(calibration["D"]).reshape((5,1))

    # Generate the object points for the checkerboard
    objp = np.zeros((6*8,3), np.float32)
    objp[:,:2] = np.mgrid[0:8,0:6].T.reshape(-1,2)

    # Create unit axes for x, y, and z axes
    mag = 5  # length of the axes
    axis = np.float32([[1,0,0], [0,1,0], [0,0,1]]).reshape(-1,3) * mag

    img = cv2.imread(str(args.checkerboard_image))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    success, corners = cv2.findChessboardCorners(gray, (8,6))

    if success:
        # Search criteria to search for refined corner pixels
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        corners2 = cv2.cornerSubPix(gray, corners,(11,11),(-1,-1), criteria)
        # Find the rotation and translation vectors.
        ret, rvecs, tvecs = cv2.solvePnP(objp, corners2, K, D)
        # project 3D points to image plane
        imgpts, jac = cv2.projectPoints(axis, rvecs, tvecs, K, D)
        # Draw the coordinate axes and the checkerboard points
        img = draw_axes(img, corners2.astype(int), imgpts.astype(int))
        # Draw the principal points and the associated camera frame axes on the image plane
        img = cv2.line(img, tuple(pp.astype(int).ravel()), tuple(pp.astype(int).ravel() + (100, 0)), (0,0,255), 3)
        img = cv2.line(img, tuple(pp.astype(int).ravel()), tuple(pp.astype(int).ravel() + (0, 100)), (0, 255, 0), 3)
        img = cv2.drawMarker(img, tuple(pp.astype(int).ravel()), (255,0,0), cv2.MARKER_SQUARE, 3, 3)
        cv2.imshow('img',img)
        k = cv2.waitKey(0) & 0xFF
        # Press s to save the image
        if k in [ord("s"), ord("S")]:
            cv2.imwrite(str(args.checkerboard_image.with_name("annotated_"+args.checkerboard_image.name)), img)

    cv2.destroyAllWindows()
